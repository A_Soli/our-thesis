% Chapter Template

\chapter{OpenCV module and Detection Algorithms} % Main chapter title
\label{ch-cv} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}
%-----------------------------------
%	SECTION 1
%-----------------------------------
\section{Motivation}

Robotic swarm systems usually vary in their complexity, and it is becoming common to have more powerful robotic members that can supervise and sometimes guide the rest of the swarm such as UAVs (Unmanned Aerial Vehicle) i.e. drones, equipped with a vision system \cite{cv-1}.
\\ \\
For this project, we choose to implement a stationary camera module fixed in an overhead position to record and take notes for what we’re happening while the system is running. This is feasible because in out scenario the robots are moving in a determined fixed area that can be monitored from a stationary point.
%-----------------------------------
%	SECTION 2
%-----------------------------------
\section{System design}

The proposed Monitoring system (Figure \ref{cv-systemdesign}) is based on the robotic system at-hand specific nature, it consists of a pipeline. On each stage of the pipeline, we tried different alternatives. The choices made might not be optimal in terms of precision in general, but they were the most suitable for the robots state at each point of time during our work. Each phase of this pipeline is discussed in details through the upcoming points of this chapter \cite{cv-2}.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.25\textwidth]{figures/cv-systemdesign}
	\caption{Monitoring System Block Diagram}
	\label{cv-systemdesign}
\end{figure}
%-----------------------------------
%	SECTION 3
%-----------------------------------
\section{Computer Vision Steps}

There are some commonly known steps for developing a CV (Computer Vision) application (Figure \ref{cv-devsteps}). All those steps have been executed while building the proposed monitoring module for the two kinds of robots used during the project and the many algorithms we tried during the main system development.
\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{figures/cv-devsteps}
	\caption{CV Common Development Steps}
	\label{cv-devsteps}
\end{figure}
%-----------------------------------
%	SECTION 4
%-----------------------------------
\section{Hardware}

The Hardware used is responsible for image capturing with an attached camera which is the first step in the proposed system pipeline (Figure \ref{cv-systemdesign}). Also, It is responsible for running the computer vision application during the swarm run-time.
%-----------------------------------
\subsection{Chosen Hardware}
Eventually, since Raspberry Pi 3 (Figure \ref{cv-rpi3}) gives a better performance compared to its price and have been widely used for similar tasks and is known to be OpenCV friendly, it was chosen due to the aforementioned reasons.
The Raspberry Pi 3 is coupled with the Arducam Mini camera module  (Figure \ref{cv-camera}).
\\
Raspberry Pi 3 has been a significant upgrade over the Raspberry Pi 2. With 1.2 GHz quad-core ARM Cortex A53, Raspberry Pi 3 is the go-to device for traditional computer vision on budget applications (usually using OpenCV \cite{cv-3}). It can output videos at full 1080p resolution. Not only has Google hinted on betting on Raspberry Pi 3, it has also provided TensorFlow support for it \cite{cv-4}. An active and growing community and recent interest shown by Google makes this a real viable choice for computer vision.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{figures/cv-rpi3}
	\caption{Raspberry Pi 3}
	\label{cv-rpi3}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/cv-camera}
	\caption{Arducam Module}
	\label{cv-camera}
\end{figure}
%-----------------------------------
\subsection{Hardware Specifications}
\begin{table}[H]
	\centering
	\caption{Raspberry Pi 3 Hardware Specifications}
	\begin{tabularx}{\linewidth}{|l|X|}
		\hline
		\textbf{SoC} & Broadcom BCM2837 \\
		\hline
		\textbf{CPU} & Quad-core ARM Cortex-A53 clocked @ 1.2GHz \\
		\hline
		\textbf{GPU} & Broadcom VideoCore IV \\
		\hline
		\textbf{RAM} & 1GB LPDDR2 (900 MHz) \\
		\hline
		\textbf{Networking} & 10/100 Ethernet, 2.4GHz 802.11n W-LAN, Bluetooth 4.1 and Bluetooth LE (Low Energy)\\
		\hline
		\textbf{Ports} &  HDMI, 3.5mm analogue audio-video jack, 4x USB 2.0, Ethernet, Camera Serial Interface (CSI), Display Serial Interface (DSI) \\
		\hline
		\textbf{Storage} & MicroSD \\
		\hline
	\end{tabularx}
\end{table}

\begin{table}[H]
	\centering
	\caption{Arducam Hardware Specifications}
	\begin{tabularx}{\linewidth}{|l|X|}
		\hline
		\textbf{Still resolution} & 5 Megapixels \\
		\hline
		\textbf{Max video resolution} &  1080p at 30 FPS  (Frame Per Second) \\
		\hline
		\textbf{Sensor} &  OmniVision OV5647 \\
		\hline
		\textbf{Sensor resolution} & 2592 x 1944 pixels \\
		\hline
		\textbf{Angle of View} & 54 x 41 degrees\\
		\hline
		\textbf{Field of View} &  2.0m x 1.33m at 2m height \\
		\hline
	\end{tabularx}
\end{table}
%-----------------------------------
%	SECTION 5
%-----------------------------------
\section{Detection Algorithms}

Detection is responsible for finding areas at which the objects of interest (robots) exist in the image. It is the second phase of the proposed system pipeline (Figure \ref{cv-systemdesign}).
\\
Object detection is one of the fundamental tasks in computer vision. A common paradigm to address this problem is to train object detectors which operate on a sub-image and apply these detectors in an exhaustive manner across all locations and scales.
%-----------------------------------
\subsection{Template Matching}
Template Matching is a method for searching and finding the location of a template image in a larger image. OpenCV comes with a function \begin{lstlisting}[language=Python,numbersep=0pt,resetmargins=true]
cv2.matchTemplate()
\end{lstlisting}
for this purpose. It simply slides the template image over the input image (as in 2D convolution) and compares the template and patch of input image under the template image. Several comparison methods are implemented in OpenCV. (You can check docs for more details). It returns a grayscale image, where each pixel denotes how much does the neighborhood of that pixel match with the template.
\\
If the input image is of size (WxH) and template image is of size (wxh), the output image will have a size of (W-w+1, H-h+1). Once you got the result, you can use
\begin{lstlisting}[language=Python,numbersep=0pt,resetmargins=true]
cv2.minMaxLoc()
\end{lstlisting}
function to find where is the maximum/minimum value. Take it as the top-left corner of the rectangle and take (w,h) as width and height of the rectangle. That rectangle is your region of the template.\cite{cv-5}
%-----------------------------------
\subsection{Contours Detection}
Contours can be explained simply as a curve joining all the continuous points (along the boundary), having same color or intensity. The contours are a useful tool for shape analysis and object detection and recognition \cite{cv-6}.
%-----------------------------------
\subsection{Haar-Cascade}
This approach is commonly used with robots since they had a fixed shape, but with the custom-made robots, it was infeasible to collect a sufficient dataset with the final shape of the robots early enough for training and testing the resulting classifier and tuning its runtime parameters.

\begin{description}
\item[Idea]
\ \\
Object Detection using Haar feature-based cascade classifiers is an effective object detection method proposed by Paul Viola and Michael Jones in 2001 \cite{cv-7}. It is a machine-learning-based approach where a cascade function is trained from a large dataset of positive (object exist in the image) and negative images (object doesn’t exist in the image). It is then used to detect objects in other images.
\\
Initially, the algorithm needs a lot of positive images (images of robots) and negative images (images without robots) to train the classifier. Then we need to extract features (Figure \ref{cv-features-types}) from it. For this, Haar classifiers are used. They are like OpenCV’s convolution-dependent kernel. Each feature is a single value obtained by subtracting the sum of pixels under white rectangle from the sum of pixels under black rectangle.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/cv-features-types}
	\caption{Types of features in images}
	\label{cv-features-types}
\end{figure}

Now all possible sizes and locations of each kernel are used to calculate plenty of features. (Even a 24x24 window results over 160000 features). For each feature calculation, the sum of pixels under white and black rectangles needs to be calculated. To solve this, Paul Viola and Michael Jones introduced the integral images. It simplifies calculation of the sum of pixels, no matter how large the number of pixels may be, to an operation involving just four pixels.
\\
The final classifier is a weighted sum of these weak classifiers. They are called weak because each alone can’t classify the image, but together they form a strong classifier. The aforementioned paper mentions that even 200 features provide detection with 95\% accuracy. Their final setup had around 6000 features. (Imagine a reduction from 160000+ features to 6000 features. This is considered a big gain).
\\
The cascade training process involves two types of trade-offs. In most cases, classifiers with more features will achieve higher detection rates and lower false positive rates. At the same time, classifiers with more features require more time to compute. In principle, one could define an optimization framework in which the number of classifier stages, the number of features in each stage and the threshold of each stage are traded off to minimize the expected number of evaluated features. Unfortunately finding this optimum is a tremendously difficult problem.
\\
OpenCV is equipped with a trainer as well as a detector. The classifier can be trained for any object like a car, a plane, etc. \cite{cv-7}.  A classifier for the kilobots was made using OpenCV.
\end{description}
%-----------------------------------
\subsection{Color-based Detection}
Since the custom-made robots and the environment have easily distinguished colors, it is possible to detect and track the robots and surroundings based on HSV color range values \cite{cv-8}.
\\
HSV space means \textbf{H}ue, \textbf{S}aturation, and \textbf{V}alue. HSV space is more suitable for human eye vision because in HSV space, the three parameters are separate, and they change in a comparable steady range. Hue and Saturation are separated in HSV space, so it is often used in image segmentation.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.3\textwidth]{figures/cv-hue-wheel}
	\caption{HSV Hue color wheel}
\end{figure} 

\begin{description}
	\item[Changing color spaces]
	\ \\
	There are more than 150 color-space conversion methods available in OpenCV.
	But the two most widely used ones were used (BGR <-> Gray and BGR <-> HSV).
	
	\ \\
	The algorithm's main idea (Figure \ref{cv-hsv-algo2}) is derived from the algorithm described by Du Xin and Zhao Xiaoguang in \cite{cv-8} (Figure \ref{cv-hsv-algo1}) which is a method based on local HSV image and the shape of the object to be recognized is proposed for robot tracking. After the color segmentation, the knowledge of the shape of objects is used to recognize objects.
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.6\textwidth]{figures/cv-hsv-algo1}
		\caption{Tracking process diagram}
		\label{cv-hsv-algo1}
	\end{figure} 
	\begin{figure}[H]
		\centering
		\includegraphics[width=0.5\textwidth]{figures/cv-hsv-algo2}
		\caption{Derived algorithm}
		\label{cv-hsv-algo2}
	\end{figure} 
\end{description}
%-----------------------------------
\subsection{Convolutional Neural Network (CNN)}
Deep Region-Based Convolution Neural Networks(R-CNNs) (Figure \ref{cv-nn}) have recently achieved state-of-the-art performance on several image recognition benchmarks, including the ImageNet \cite{cv-9}. They commonly run on GPUs achieving a fast enough performance.
\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{figures/cv-nn}
	\caption{Region-Based Convolution Neural Networks}
	\label{cv-nn}
\end{figure}

\begin{description}
\item[Idea]
\ \\
An object detection problem can be approached as either a classification problem or a regression problem. As a classification problem, the image is divided into small patches, each of which will be run through a classifier to determine whether there are objects in the patch. Then the bounding boxes will be assigned to locate around patches that are classified with a high probability of the presence of an object. \\
In the regression approach, the whole image will be run through a convolutional neural network to directly generate one or more bounding boxes for objects in the images.
\end{description}
%-----------------------------------
\subsection{One Shot Learning}
Conventional wisdom says that deep neural networks are very good at learning from high dimensional data like images or spoken language, but only when they have huge amounts of labeled examples to train on. Humans, on the other hand, are capable of one-shot learning - if you take a human who’s never seen a spatula before, and show them a single picture of a spatula, they will probably be able to distinguish spatulas from other kitchen utensils with astoundingly high precision.

Yet another one of the things humans can do that seemed trivial to us right up until we tried to make an algorithm do it.
\\
This ability to rapidly learn from very little data seems like it’s obviously desirable for machine learning systems to have because collecting and labeling data is expensive. We also think this is an important step on the long road towards general intelligence.
\\
Recently there have been many interesting papers about one-shot learning with neural nets and they’ve gotten some good results. This is a new area that really excites me, so we wanted to make a gentle introduction to make it more accessible to fellow newcomers to deep learning. \cite{cv-11}
%-----------------------------------
%	SECTION 6
%-----------------------------------
\section{Using Camera as a Positioning System}

As a result of building an indoor project prototype, the GPS sensor wasn’t a feasible choice, so the alternative solution was using the camera with the detection concept to locate the robots, recognize them then send them their positions and angles to help them reach their goals.
\\
So, the need for a unique identity for each robot was urgent. Choosing the shape of the patterns was also a challenge because of many aspects. For instance, the small size of the detected patterns due to the large distance between the camera and the arena also the fact that the patterns are custom made and there are no available datasets for them which makes using any kind of algorithms that have training phase isn’t an option because that leads to low accuracy output and that isn’t compatible with real-time application like swarm robotics.
\\
The shape, color, and size of patterns had been changed several times due to the cons of each trial with different algorithms (Figure \ref{cv-patterns-versions}).
\\ \\ \\
\begin{figure}[H]
	\centering
	\begin{subfigure}[b]{0.3\textwidth}
		\centering
		\includegraphics[width=0.3\linewidth]{figures/cv-patterns1}
		\caption{version 1}
		\label{cv-patterns-1}
	\end{subfigure}%
	\begin{subfigure}[b]{0.3\textwidth}
		\centering
		\includegraphics[width=0.3\linewidth]{figures/cv-patterns2}
		\caption{version 2}
		\label{cv-patterns-2}
	\end{subfigure}%
	\begin{subfigure}[b]{0.3\textwidth}
		\centering
		\includegraphics[width=0.3\linewidth]{figures/cv-patterns3}
		\caption{version 3}
		\label{cv-patterns-3}
	\end{subfigure}
	\caption{Patterns}
	\label{cv-patterns-versions}
\end{figure}

$\bullet$ \textbf{Trials and Results}
\\
We used many approaches to detect and clearly classify the patterns, but there were some obstacles due to patterns’ shapes or the computational power and resources (GPU).

\begin{description}
\item[Template Matching]
\
\begin{lstlisting}[language=Python,numbersep=0pt,resetmargins=true]
templates = [cv2.imread('p1.jpg',0),cv2.imread('p2.jpg',0),cv2.imread('p3.jpg',0),cv2.imread('p4.jpg',0),cv2.imread('p5.jpg',0),cv2.imread('p6.jpg',0),cv2.imread('p7.jpg',0),cv2.imread('p8.jpg',0)]
\end{lstlisting}

\begin{lstlisting}[language=Python,numbersep=0pt,resetmargins=true]
w, h = template.shape[::-1]
res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
top_left = max_loc
bottom_right = (top_left[0] + w, top_left[1] + h)
pos[i] = (top_left[0] + w/2 , top_left[1] + h/2)
cv2.rectangle(frame,top_left, bottom_right, 255, 2)
\end{lstlisting}

\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{figures/cv-temp-match-out}
	\caption{Template matching output}
	\label{cv-temp-match}
\end{figure}
%-----------------------------------
\item[Haar-Cascade]
\ \\
Trial with the Haar Cascade Classifier was carried out with our custom-made patterns which are attached to our RowBot swarm in order to classify, locating them. \\
A dataset of 2880 different image 360 for each pattern. (Figure 6.9) And for each image of them, OpenCV was used to create samples (opencv\_createsamples API) to generate 50 different distorted images by adding noise or rotation. Using that dataset and concatenating their annotation files, then using OpenCV to create samples (using opencv\_createsamples) again to create the vector file, then using opencv\_traincascade to train a 15 stage Local Binary Patterns feature-type classifier, where the main difference between local binary and Haar features is the representation of feature values using integers instead of floating points.
\\
The training command,
\begin{lstlisting}[language=bash,numbersep=0pt,resetmargins=true]
opencv\_traincascade -data data -vec positives.vec -bg bg.txt -numPos 2200 -numNeg 1000 -numStages 15 -w 50 -h 50 -featureType LBP
\end{lstlisting}
houses a set of parameters as, 
\begin{itemize}
	\item \textit{-vec positives} Which is the name of the file containing the positive samples for training (output from opencv\_createsamples second run)
	
	\item \textit{-data data} Which is the directory where the trained classifier should be stored. This folder should be created manually beforehand.
	
	\item \textit{-bg bg.txt} Background description file. This is the file containing the negative sample images.
	
	\item \textit{-numPos 2200} Number of positive samples used in training.
	
	\item \textit{-numNeg 1000} Number of negative samples used in training.
	
	\item \textit{-numStages 15} Number of cascade stages to be trained.
	
	\item \textit{-featureType LBP} Type of features LBP for local binary patterns.
	
	\item \textit{-w 50} Width of training samples (in pixels).
	
	\item \textit{-h 50} Height of training samples (in pixels).
	
\end{itemize}

Then, we apply the cascade files containing trained features to the video stream to get the patterns classified.
\begin{lstlisting}[language=Python,numbersep=0pt,resetmargins=true]
cascade = cv2.CascadeClassifier('cascade.xml')

patterns = cascade.detectMultiScale(frame, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30))
\end{lstlisting}
\comment{
\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{figures/cv-p8}
	\caption{Haar-cascade output}
	\label{cv-p8}
\end{figure}
}
%-----------------------------------
\item[CNN: YOLO]
\ \\
Trial with YOLO was carried out with our custom-made patterns which are attached to our RowBot swarm in order to classify, locating them.
\\
A dataset of 2880 different image 360 for each pattern (Figure 6.9).  And for each image of them, OpenCV was used to extract the annotations. Then, using that dataset and concatenating their annotation files to get the weights file which is used to validate and test the captured stream.\cite{cv-10}

\begin{lstlisting}[language=bash,numbersep=0pt,resetmargins=true]
flow --model cfg/tiny-yolo-voc.cfg --load bin/tiny-yolo-voc.weights --train --annotation MyTest/annotations --dataset MyTest/Patterns/All
\end{lstlisting}
houses a set of parameters as,
\begin{itemize}
	\item \textit{--model cfg/tiny-yolo-voc.cfg} YOLO model configurations.
	
	\item \textit{--load bin/tiny-yolo-voc.weights} Load pre-trained weights file.
	
	\item \textit{--train --annotation MyTest/annotations} Annotation files consist of the tagged patterns themselves, positions, heights, and widths in the dataset.
	
	\item \textit{--dataset MyTest/Patterns/All} Access the dataset directory, then combine the annotations with the dataset.
\end{itemize}
Then, apply the trained weights to the camera stream and draw bounding boxes around each pattern. As follows,
\begin{lstlisting}[language=bash,numbersep=0pt,resetmargins=true]
flow --model cfg/yolo-new.cfg --load bin/yolo-new.weights --demo camera
\end{lstlisting}

However, we couldn’t apply this method because of the lack of high-performance resources like GPU.
%-----------------------------------
\item[CNN: One Shot Learning]
\
\begin{itemize}
	\item \textbf{Labeling}
	set a list of ones/zeros corresponding to each pattern.
	\item \textbf{Identify Train/Test sets}
	\begin{lstlisting}[language=Python,numbersep=0pt,resetmargins=true]
	training_data.append([np.array(imgs_in_train_directory), np.array(label)])
	testing_data.append([np.array(imgs_in_test_directory), img_num])
	\end{lstlisting}
	\item \textbf{Create model’s structure (Neural Network)}
	We have 5 relu-activation layer with max-pool after each one, and 2 fully connected layers with a drop-out layer between them.
	The loss function is cross-entropy, and the optimizer is Adam.
	\begin{lstlisting}[language=Python,numbersep=0pt,resetmargins=true]
	convnet = input_data(shape =[None, IMG_SIZE, IMG_SIZE, 1], name ='input')
	
	convnet = conv_2d(convnet, 32, 5, activation ='relu')
	convnet = max_pool_2d(convnet, 5)
	
	convnet = conv_2d(convnet, 64, 5, activation ='relu')
	convnet = max_pool_2d(convnet, 5)
	
	convnet = conv_2d(convnet, 128, 5, activation ='relu')
	convnet = max_pool_2d(convnet, 5)
	
	convnet = conv_2d(convnet, 64, 5, activation ='relu')
	convnet = max_pool_2d(convnet, 5)
	
	convnet = conv_2d(convnet, 32, 5, activation ='relu')
	convnet = max_pool_2d(convnet, 5)
	
	convnet = fully_connected(convnet, 1024, activation ='relu')
	convnet = dropout(convnet, 0.8)
	
	convnet = fully_connected(convnet, 8, activation ='softmax')
	
	convnet = regression(convnet, optimizer ='adam', learning_rate = LR,
	loss ='categorical_crossentropy', name ='targets')
	
	model = tflearn.DNN(convnet, tensorboard_dir ='log')
	\end{lstlisting}
	\item \textbf{Train model}
	\begin{lstlisting}[language=Python,numbersep=0pt,resetmargins=true]
	model.fit({'input': X}, {'targets': Y}, n_epoch = 5,validation_set =({'input': test_x}, {'targets': test_y}),snapshot_step = 500, show_metric = True, run_id = MODEL_NAME)
	\end{lstlisting}
	\item \textbf{Predict classes (patterns) using model}
	\begin{lstlisting}[language=Python,numbersep=0pt,resetmargins=true]
	model_out = model.predict([data])[0]
	\end{lstlisting}
\end{itemize}
\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{figures/cv-oneshot-out}
	\caption{One shot training output}
	\label{cv-oneshot-out}
\end{figure}
%-----------------------------------
\item[Contours Detection]
\ \\
Contour detection algorithm trying to find the connected points. So, in order to find our patterns the algorithm try to track the shapes on the captured frames until get the full true pattern that match the dataset.
\begin{itemize}
	\item \textbf{Contour Detection \Romannum{1}}
	\\
	The fact that our old version of patterns (Figure \ref{cv-patterns-1}) were just a combination of rectangles make the pattern extraction much easier.
	First, we need to get some values as the threshold.
	\begin{lstlisting}[language=Python,numbersep=0pt,resetmargins=true]
	ret,thresh = cv2.threshold(imgray,127,255,0)
	\end{lstlisting}
	Then, finding the patterns contours by passing those values which are extracted from the captured frames.
	\begin{lstlisting}[language=Python,numbersep=0pt,resetmargins=true]
	image,contours,hierarchy=cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
	\end{lstlisting}
	\textit{cv2.CHAIN\_APPROX\_SIMPLE} removes all redundant points and compresses the contour, thereby saving memory.
	
	The shapes of the pattern were an obstruction to reach a high performance by using the Contour detection technique. But the concept was useful anyway to get important information like dimensions, center, and the angle.
	
	The angle of the robot can be detected by detecting the angle of a reference shape which is constant for all patterns (Figure \ref{cv-patterns-1}).
	\begin{lstlisting}[language=Python,numbersep=0pt,resetmargins=true]
	def get_dims(crnr_pts):
		[P1, P2, P3, P4] = crnr_pts
		w = math.sqrt(pow(P1[0] - P2[0],2) + pow(P1[1] - P2[1],2))
		h = math.sqrt(pow(P1[0] - P4[0],2) + pow(P1[1] - P4[1],2))
		c = (P1[0]+(w/2), P1[1]+(h/2))
		return [w, h, c]
	\end{lstlisting}
	The \textit{get\_dims()} function is about taking the calculated corners from get\_pts() and by comparing the points and do some math we can get the width, height and center.
	\begin{lstlisting}[language=Python,numbersep=0pt,resetmargins=true]
	def get_angle(p2,p1):
		(x2, y2), (x1, y1) = p2, p1
		slope = (y2 - y1)/(x2 - x1)
		angle =  math.degrees(math.atan(slope)) + 90
		return angle
	\end{lstlisting}
	The \textit{get\_angle()} function uses two corners points and calculate the angle between them using tan concept.
	
	Those results were used for the final model but the detection itself needs a huge improvement to get more accurate results.	

	\item \textbf{Contour Detection \Romannum{2}}
	\\
	In the second version, we try to change the shape of the patterns and make their shapes way different which was expected to make the detection much easier (Figure \ref{cv-patterns-2}). So, the Algorithm performs in a big while loop in order to search for the shape of the pattern.
	\begin{lstlisting}[language=Python,numbersep=0pt,resetmargins=true]
	cnts = cv2.findContours(inverted.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
	\end{lstlisting}
	Then, inside a for loop we were try to get the dimensions and the for corners. After that, drawing a box show the detected shape.
	\begin{lstlisting}[language=Python,numbersep=0pt,resetmargins=true]
	rect = np.zeros((4, 2), dtype = "float32")
	rect[1] = [approx[0][0][0],approx[0][0][1]]
	rect[0] = [approx[1][0][0],approx[1][0][1]]
	rect[3] = [approx[2][0][0],approx[2][0][1]]
	rect[2] = [approx[3][0][0],approx[3][0][1]]
	\end{lstlisting}
	
	This version gives much better performance, but it wasn’t enough for a real time application as the swarm project. The small error in detecting a one robot can lead in miss- locating or wrong angle which could destroy the whole application as the swarm concept always used in applications needs a high performance.
	\\
	\item \textbf{Contour Detection \Romannum{3}}
	\\
	In this version we just change the patterns to make the shapes different
	but more formal. In addition to adding colors to them (Figure \ref{cv-patterns-3}), so we can combine the contour detection concept with color detection method in order to improve the performance.
	
	The color detection concept depends on uses the RGB values for each color and define a range of values for each pattern. Avoiding specifying an accurate value for each pattern and replace it with a range reduce the error value.
	\\
	Just adding the \textit{check\_color()} to the previous code was enough.
	\begin{lstlisting}[language=Python,numbersep=0pt,resetmargins=true]
	def check_color(image):
		image = cv2.resize(image, (250,250))
		boundaries = [
		([31, 75, 0], [65, 160, 10]),       #P1
		([7, 0, 113], [20, 10, 230]),       #P2
		([70, 0, 118], [150, 0, 250]),      #P3
		([113, 86, 0], [230, 170, 10]),     #P4
		([75, 0, 50], [160, 255, 105]),     #P5
		([75, 100, 125], [160, 210, 255]),  #P6
		([86, 31, 4], [140, 40, 50]),       #P7
		([0, 118, 127], [10, 240, 255])     #P8
		]
		
		# loop over the boundaries
		output = {}
		patterns = []
		n = 1
		for (lower, upper) in boundaries:
			# create NumPy arrays from the boundaries
			lower = np.array(lower, dtype = "uint8")
			upper = np.array(upper, dtype = "uint8")
			
			# find the colors within the specified boundaries and apply
			# the mask
			mask = cv2.inRange(image, lower, upper)
			out_img = cv2.bitwise_and(image, image, mask = mask)
			output[n] = out_img
			n += 1
		# Loop over detected colours and get the most similar one
		for i, im in output.items():
			average_color = [im[:,:,j] for j in range(im.shape[-1])]
			if (np.sum(average_color)/im.shape[-1]) > 100000:
				patterns.append(i)
		return patterns
	\end{lstlisting}
	So, combining a shape and RGB range for each pattern really work in improving the performance significantly.
\end{itemize}
\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{figures/cv-contours-out}
	\caption{Contours detection output}
	\label{cv-contours-out}
\end{figure}
\end{description}