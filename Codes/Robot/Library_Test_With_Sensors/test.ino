#include <Servo.h>
#include <SPI.h>
#include <Ultrasonic.h>
#include <Robot.h>
#define CE_PIN   9
#define CSN_PIN 10

Ultrasonic ultrasonic(A4,A5);

int sensor;
int state=0;
Robot rowbot(0);

void setup() {
  //radio.begin();
  //radio.openWritingPipe(pipe);
  pinMode(7,OUTPUT);
  pinMode(8,OUTPUT);
  digitalWrite(7,1);
  digitalWrite(8,0);
  state=0;

  rowbot.left.attach(5); 
  rowbot.right.attach(6);
  rowbot.setSpeed1(40); 
  if(rowbot.getNum() == 0 ){
    rowbot.setisleader(true);
    }  
}

void loop() {
  if(rowbot.getisleader()==true){
    sensor = ultrasonic.Ranging(CM);
    if(sensor>10&&state==0){
      rowbot.Stop();
      delay(1000);
      }
    if(sensor<10){
      rowbot.goLeft(400);
      //delay(400);
      state=1;
      }
      if(sensor>10&&state==1){
        rowbot.forward();
        }
  }
  else{
    sensor = ultrasonic.Ranging(CM);
    if (sensor > 10)
    {
      rowbot.forward();
    }
    else if (sensor <= 10)
    {
      rowbot.goRight(20);
      delay(400);
      rowbot.forward();
    }
    }
  
}
