# import the necessary packages
from collections import deque
import numpy as np
import argparse
import imutils
import cv2
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
from matplotlib import pyplot as plt

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video",
	help="path to the (optional) video file")
ap.add_argument("-b", "--buffer", type=int, default=64,
	help="max buffer size")
args = vars(ap.parse_args())

# if a video path was not supplied, grab the reference
# to the webcam
if not args.get("video", False):
    #camera = cv2.VideoCapture(0)
    camera = PiCamera()
    camera.resolution = (640, 480)
    camera.framerate = 20
    rawCapture = PiRGBArray(camera, size=(640, 480))
    # allow the camera to warmup
    time.sleep(0.1)
 
# otherwise, grab a reference to the video file
else:
    camera = cv2.VideoCapture(args["video"])

# keep looping
for image in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
	# grab the raw NumPy array representing the image, then initialize the timestamp
	# and occupied/unoccupied text
    frame = image.array
    
   	# if we are viewing a video and we did not grab a frame,
	# then we have reached the end of the video
    #if args.get("video") and not grabbed:
    #    break
    # resize the frame, blur it, and convert it to the HSV
    # color space
    frame = imutils.resize(frame, width=600)
    img_rgb = frame
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
    templates = [cv2.imread('Patterns/p1.jpg',0),cv2.imread('Patterns/p2.jpg',0),cv2.imread('Patterns/p3.jpg',0),cv2.imread('Patterns/p4.jpg',0),cv2.imread('Patterns/p5.jpg',0),cv2.imread('Patterns/p6.jpg',0),cv2.imread('Patterns/p7.jpg',0),cv2.imread('Patterns/p8.jpg',0)]
    for i in range(len(templates)):
        template = templates[i]
        color = (10*(i+1),15*(i+1),20*(i+1))
        w, h = template.shape[::-1]
        res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
        top_left = max_loc
        bottom_right = (top_left[0] + w, top_left[1] + h)
        pos = {}
        pos[i] = (top_left[0] + w/2 , top_left[1] + h/2)
        cv2.rectangle(frame,top_left, bottom_right, 255, 2)
        cv2.imshow('res',frame)
        if res.any():
            print('pos[pattern'+ str(i+1)+'] = ' + str(pos[i]))
    key = cv2.waitKey(1) & 0xFF
    rawCapture.truncate(0) 
	# if the 'q' key is pressed, stop the loop
    if key == ord("q"):
        break
# cleanup the camera and close any open windows
camera.release()
cv2.destroyAllWindows()
#print(loc)

