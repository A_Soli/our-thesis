# Swarm Intelligence in Swarm Robotics


## Description
   Swarm robots aims to achieve some tasks with the cooperation of many robots.
   <!-- [Here](hyperlink) -->
   ("Figures/Rowbots.jpg")

## Technologies used
   1. OpenCV python.
   2. Tensorflow.
   3. Arduino.

   <!-- ```python
   ...
   ...
   ``` -->