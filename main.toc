\babel@toc {english}{}
\contentsline {chapter}{Abstract}{i}{section*.1}
\contentsline {chapter}{Acknowledgements}{ii}{section*.4}
\contentsline {chapter}{Contents}{iii}{chapter*.5}
\contentsline {chapter}{List of Figures}{vi}{chapter*.6}
\contentsline {chapter}{List of Tables}{viii}{chapter*.7}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.8}
\contentsline {section}{\numberline {1.1}State of the art}{1}{section.9}
\contentsline {section}{\numberline {1.2}Swarm Intelligence}{1}{section.10}
\contentsline {subsection}{\numberline {1.2.1}Particle Swarm Optimization}{2}{subsection.11}
\contentsline {subsection}{\numberline {1.2.2}Ant Colony Optimization}{3}{subsection.12}
\contentsline {subsection}{\numberline {1.2.3}Swarm Intelligence Applications}{3}{subsection.13}
\contentsline {section}{\numberline {1.3}Swarm Robotics}{4}{section.14}
\contentsline {subsection}{\numberline {1.3.1}Criteria for Swarm Robotics}{4}{subsection.15}
\contentsline {subsection}{\numberline {1.3.2}Advantages to Classical Approaches}{5}{subsection.16}
\contentsline {subsection}{\numberline {1.3.3}Swarm Robotics Applications}{5}{subsection.17}
\contentsline {section}{\numberline {1.4}Introduction to Swarm}{6}{section.18}
\contentsline {section}{\numberline {1.5}Thesis Organization}{9}{section.19}
\contentsline {chapter}{\numberline {2}Robot}{11}{chapter.20}
\contentsline {section}{\numberline {2.1}Motivation}{11}{section.21}
\contentsline {section}{\numberline {2.2}Hardware}{12}{section.24}
\contentsline {subsection}{\numberline {2.2.1}Frame}{14}{subsection.27}
\contentsline {subsection}{\numberline {2.2.2}PCB Design and Components}{15}{subsection.28}
\contentsline {subsection}{\numberline {2.2.3}Batteries}{17}{subsection.32}
\contentsline {subsection}{\numberline {2.2.4}Positioning System}{18}{subsection.33}
\contentsline {subsection}{\numberline {2.2.5}Sensors}{18}{subsection.34}
\contentsline {subsubsection}{\numberline {2.2.5.1}Ultrasonic Sensor}{18}{subsubsection.35}
\contentsline {section}{\numberline {2.3}Software}{18}{section.36}
\contentsline {chapter}{\numberline {3}Control Strategy}{21}{chapter.100}
\contentsline {section}{\numberline {3.1}Motivation}{21}{section.101}
\contentsline {section}{\numberline {3.2}Tracking}{21}{section.104}
\contentsline {subsection}{\numberline {3.2.1}Intro}{21}{subsection.105}
\contentsline {subsection}{\numberline {3.2.2}Tracking methods}{23}{subsection.109}
\contentsline {subsubsection}{\numberline {3.2.2.1}Dead reckoning}{23}{subsubsection.110}
\contentsline {subsubsection}{\numberline {3.2.2.2}Bresenham\IeC {\textquoteright }s line algorithm}{24}{subsubsection.112}
\contentsline {section}{\numberline {3.3}Leadership Selection}{28}{section.116}
\contentsline {chapter}{\numberline {4}OpenCV module and Detection Algorithms}{30}{chapter.144}
\contentsline {section}{\numberline {4.1}Motivation}{30}{section.145}
\contentsline {section}{\numberline {4.2}System design}{30}{section.146}
\contentsline {section}{\numberline {4.3}Computer Vision Steps}{31}{section.148}
\contentsline {section}{\numberline {4.4}Hardware}{31}{section.150}
\contentsline {subsection}{\numberline {4.4.1}Chosen Hardware}{31}{subsection.151}
\contentsline {subsection}{\numberline {4.4.2}Hardware Specifications}{33}{subsection.154}
\contentsline {section}{\numberline {4.5}Detection Algorithms}{33}{section.157}
\contentsline {subsection}{\numberline {4.5.1}Template Matching}{33}{subsection.158}
\contentsline {subsection}{\numberline {4.5.2}Contours Detection}{34}{subsection.163}
\contentsline {subsection}{\numberline {4.5.3}Haar-Cascade}{34}{subsection.164}
\contentsline {subsection}{\numberline {4.5.4}Color-based Detection}{35}{subsection.166}
\contentsline {subsection}{\numberline {4.5.5}Convolutional Neural Network (CNN)}{37}{subsection.170}
\contentsline {subsection}{\numberline {4.5.6}One Shot Learning}{38}{subsection.172}
\contentsline {section}{\numberline {4.6}Using Camera as a Positioning System}{38}{section.173}
\contentsline {chapter}{\numberline {5}Communication Module}{47}{chapter.292}
\contentsline {section}{\numberline {5.1}Motivation}{47}{section.293}
\contentsline {section}{\numberline {5.2}Hardware}{47}{section.294}
\contentsline {subsection}{\numberline {5.2.1}Robots}{49}{subsection.295}
\contentsline {subsection}{\numberline {5.2.2}Camera}{50}{subsection.296}
\contentsline {section}{\numberline {5.3}Integration All Communication}{51}{section.299}
\contentsline {subsection}{\numberline {5.3.1}Using Network Library}{51}{subsection.300}
\contentsline {subsection}{\numberline {5.3.2}Using Customized Network}{52}{subsection.303}
\contentsline {section}{\numberline {5.4}Communication Methods}{53}{section.307}
\contentsline {subsection}{\numberline {5.4.1}Pipes}{53}{subsection.308}
\contentsline {subsection}{\numberline {5.4.2}Network}{54}{subsection.311}
\contentsline {subsubsection}{\numberline {5.4.2.1}Network Library}{54}{subsubsection.312}
\contentsline {subsubsection}{\numberline {5.4.2.2}Customized Network}{59}{subsubsection.388}
\contentsline {chapter}{\numberline {6}Integration Task}{64}{chapter.480}
\contentsline {section}{\numberline {6.1}Motivation}{64}{section.481}
\contentsline {section}{\numberline {6.2}Main Task "Formation"}{65}{section.483}
\contentsline {subsection}{\numberline {6.2.1}Control module}{65}{subsection.484}
\contentsline {subsection}{\numberline {6.2.2}CV module}{65}{subsection.485}
\contentsline {subsection}{\numberline {6.2.3}Communication module}{65}{subsection.486}
\contentsline {chapter}{\numberline {A}Sensors}{66}{appendix.487}
\contentsline {section}{\numberline {A.1}Ultrasonic Sensor}{66}{section.488}
\contentsline {subsection}{\numberline {A.1.1}Specifications and Description}{66}{subsection.490}
\contentsline {subsection}{\numberline {A.1.2}Theory of Operation}{67}{subsection.493}
\contentsline {subsection}{\numberline {A.1.3}Pins Configuration}{67}{subsection.495}
\contentsline {chapter}{\numberline {B}RF Communication Module}{68}{appendix.497}
\contentsline {section}{\numberline {B.1}Specifications and Description}{68}{section.499}
\contentsline {section}{\numberline {B.2}Theory of Operation}{69}{section.500}
\contentsline {section}{\numberline {B.3}Pins Configuration}{71}{section.502}
\contentsline {chapter}{\numberline {C}Power Train}{72}{appendix.504}
\contentsline {chapter}{\numberline {D}MCU: ATmega328P}{73}{appendix.505}
\contentsline {section}{\numberline {D.1}Configuration Summary}{74}{section.507}
\contentsline {section}{\numberline {D.2}Block Diagram}{75}{section.509}
\contentsline {section}{\numberline {D.3}Pin-out}{76}{section.511}
\contentsline {chapter}{\numberline {E}Raspberry Pi3 Model B+ \& Arducam}{77}{appendix.514}
\contentsline {chapter}{\numberline {F}Source Codes}{83}{appendix.517}
\contentsline {chapter}{Bibliography}{84}{appendix.517}
